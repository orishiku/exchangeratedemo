""" Models module
"""
from django.db import models


class RateSources(models.TextChoices):
    """ Rate sources choices
    """
    DOF = 'DOF'
    FIXER = 'Fixer'
    BANXICO = 'Banxico'


class Rate(models.Model):
    """ Rate model
    """
    last_update = models.DateTimeField(auto_now_add=True)
    value = models.FloatField()
    source_date = models.DateField(auto_now_add=True)
    source = models.CharField(max_length=10, choices=RateSources.choices)

    class Meta:
        unique_together = ('source', 'source_date')

    def __str__(self):
        return f'{self.source}: {self.value}'
