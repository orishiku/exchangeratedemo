""" Source apps module
"""
from django.apps import AppConfig


class SourcesConfig(AppConfig):
    """ Source app configuration
    """
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'sources'
