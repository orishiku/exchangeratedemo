""" Fixer source module
"""
import requests
from django.conf import settings
from sources.core.base import SourceBase


class SourceFixer(SourceBase):
    """ Fixer source class
    """
    source_endpoint = 'https://api.apilayer.com/fixer/latest?base=USD&symbols=MXN'

    def _consult_rate(self) -> dict:
        data = {}
        headers = {'apikey': settings.FIXER_API_KEY}
        response = requests.request("GET",
                                    self.source_endpoint,
                                    headers=headers,
                                    timeout=20)
        content = response.json()
        if content['success']:
            data['date'] = content['date'].split('-')
            data['date'].reverse()
            data['date'] = '/'.join(data['date'])
            data['value'] = content['rates']['MXN']

        return data
