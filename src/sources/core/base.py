""" Base sources module
"""
from abc import ABC, abstractmethod

from django.utils import timezone
from django.core.exceptions import ValidationError


class SourceBase(ABC):
    """ Base class for sources
    """
    _rate = None

    def __init__(self):
        if type(self).validate_data is not SourceBase.validate_data \
                or type(self).current_rate is not SourceBase.current_rate:
            raise TypeError('"_validate_error" and "current_rate" shouldn\'t be overriden')

    @property
    @abstractmethod
    def source_endpoint(self) -> str:
        """ Source's endpoint to retrieve data from
        """

    @abstractmethod
    def _consult_rate(self) -> dict:
        """ Consults and formats data from source
        """

    @staticmethod
    def validate_data(data: dict) -> bool:
        """ Validates data format to comply with requirements
        """
        valid_date = False
        valid_value = False

        if 'date' in data.keys() and data['date'] != '':
            date = [int(part) for part in data['date'].split('/')]

            if len(date) == 3:
                try:
                    date = timezone.datetime(date[2], date[1], date[0]).astimezone()
                    valid_date = True
                except ValueError as exc:
                    raise ValidationError('Rate result invalid date') from exc

        if 'value' in data.keys() and data['value'] != '':
            try:
                float(data['value'])
                valid_value = True
            except ValueError as exc:
                raise ValidationError('Rate result is not a number') from exc

        if not valid_date or not valid_value:
            raise ValidationError('Rate result is not complete')

    def current_rate(self, force_consult: bool = False) -> dict:
        """ returns current rate from DB or remote source if needed
        """
        if self._rate is None or force_consult:
            self._rate = None
            rate = self._consult_rate()
            self.validate_data(rate)
            self._rate = rate

        return {'date': self._rate['date'], 'value': self._rate['value']}
