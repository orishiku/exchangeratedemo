""" Banxico source module
"""
import xml.etree.ElementTree as ET
import requests

from django.conf import settings
from sources.core.base import SourceBase


class SourceBanxico(SourceBase):
    """ Banxico source class for rate consult
    """
    source_endpoint = 'https://www.banxico.org.mx/SieAPIRest/' \
        + 'service/v1/series/SF43718/datos/oportuno'

    def _consult_rate(self) -> dict:
        """ Consults rate from Banxico API
        """
        data = {}
        headers = {'Bmx-Token': settings.BANXICO_API_KEY,
                   'Accept': 'application/xml'}
        response = requests.request("GET",
                                    self.source_endpoint,
                                    headers=headers,
                                    timeout=20)

        if response.status_code == 200:
            content = ET.ElementTree(ET.fromstring(response.text)).getroot()
            data['date'] = content[0].find("./Obs/fecha").text
            data['value'] = content[0].find("./Obs/dato").text

        return data
