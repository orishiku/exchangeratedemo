"""" DOF source module
"""
import requests
from bs4 import BeautifulSoup
from lxml import etree

from sources.core.base import SourceBase


class SourceDOF(SourceBase):
    """ DOF source class
    """
    source_endpoint = 'https://www.banxico.org.mx/tipcamb/tipCamMIAction.do'

    def _consult_rate(self) -> dict:
        """ Consults rate scrapping from DOF site
        """
        data = {}
        response = requests.get(self.source_endpoint, timeout=20)

        if response.status_code == 200:
            content = BeautifulSoup(response.content, "html.parser")
            dom = etree.HTML(str(content))
            table = dom.xpath("//td[contains(text(), 'Para pagos')]/../..")[0]
            for i in reversed(range(3, 5)):
                date = table.xpath(f'./tr[{i}]/td[1]')[0].text.strip()
                value = table.xpath(f'./tr[{i}]/td[3]')[0].text.strip()
                if 'N/E' not in value:
                    data['date'] = date
                    data['value'] = float(value)

        return data
