""" sources tests module
"""
# pylint: disable=E0110,W0212
from django.test import TestCase
from django.utils import timezone
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError

from sources.core.base import SourceBase
from sources.core.banxico import SourceBanxico
from sources.core.fixer import SourceFixer
from sources.core.dof import SourceDOF


def validate_endpoint(endpoint: str) -> bool:
    """ validates endpoint is an url
    """
    validator = URLValidator()
    try:
        validator(endpoint)
        return True
    except ValidationError:
        return False


class BaseSourceTestCase(TestCase):
    """ Base source tests
    """

    def test_new_source_class(self):
        """ New source classes created from BaseSource should comply
        """
        NewSourceA = type('SourceNewA', (SourceBase, ), {
            'source_endpoint': 'http://source.com',
            '_consult_rate': validate_endpoint,
            'validate_data': validate_endpoint,
            'current_rate': validate_endpoint
        })

        with self.assertRaises(TypeError):
            NewSourceA()

    def test_invalid_data_validation(self):
        """ Data validation catches invalid inputs
        """
        invalid_data = (
            {},
            {'date': ''},
            {'value': ''},
            {'value': '8a', 'date': timezone.localdate().strftime("%d/%m/%Y")},
            {'value': '8a', 'date': timezone.localdate().strftime("%Y/%m/%Y")},
            {'value': '8a',
             'date': (timezone.localdate() + timezone.timedelta(days=1)).strftime("%d/%m/%Y")},
            {'value': 5.5, 'date': '8/8'})

        for data in invalid_data:
            with self.assertRaises(ValidationError):
                SourceBase.validate_data(data)


class RateSourcesTestCase(TestCase):
    """ sources tests
    """

    def test_dof_source(self):
        """ DOF source data is correctly retrived
        """
        source = SourceDOF()
        self.assertTrue(validate_endpoint(source.source_endpoint), 'Invalid endpoint')

        rate = source.current_rate()
        self.assertTrue('date' in rate and 'value' in rate,
                        "rate dict is not complete")
        self.assertTrue(source.current_rate() == rate,
                        "previous rate consult wasn't saved")

        with self.assertRaises(ValidationError):
            source.source_endpoint = source.source_endpoint[:-2]
            source.current_rate(True)

        self.assertTrue(source._rate is None)

    def test_fixer_source(self):
        """ Fixer source data is correctly retrived
        """
        source = SourceFixer()
        self.assertTrue(validate_endpoint(source.source_endpoint), 'Invalid endpoint')

        rate = source.current_rate()
        self.assertTrue('date' in rate and 'value' in rate,
                        "rate dict is not complete")
        self.assertTrue(source.current_rate() == rate,
                        "previous rate consult wasn't saved")
        self.assertTrue(source.current_rate(True) != rate,
                        "previous rate consult wasn't saved")
        with self.assertRaises(ValidationError):
            source.source_endpoint = source.source_endpoint[:-2]
            source.current_rate(True)

        self.assertTrue(source._rate is None)

    def test_banxico_source(self):
        """ Banxico source data is correctly retrived
        """
        source = SourceBanxico()
        self.assertTrue(validate_endpoint(source.source_endpoint), 'Invalid endpoint')

        rate = source.current_rate()
        self.assertTrue('date' in rate and 'value' in rate,
                        "rate dict is not complete")
        self.assertTrue(source.current_rate() == rate,
                        "previous rate consult wasn't saved")

        with self.assertRaises(ValidationError):
            source.source_endpoint = source.source_endpoint[:-2]
            source.current_rate(True)

        self.assertTrue(source._rate is None)
