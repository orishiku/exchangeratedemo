#!/bin/sh
if [ -z ${DJANGO_SETTINGS_MODULE+x} ];then
    . $PWD/.bash_profile
fi

# Django setup
python manage.py makemigrations sources api;
python manage.py migrate --no-input;
python manage.py collectstatic --noinput --clear;

if [ -n "$DJANGO_SUPERUSER_USERNAME" ] && [ -n "$DJANGO_SUPERUSER_PASSWORD" ] && [ -n "$DJANGO_SUPERUSER_EMAIL" ];then
   python manage.py createsuperuser --no-input --email $DJANGO_SUPERUSER_EMAIL
fi

# Run server
/env/bin/gunicorn -b :8000 exchange_rate_demo.wsgi:application
