""" Tests module
"""

from http import HTTPStatus
from django.test import TestCase
from django.urls.base import reverse
from django.contrib.auth import get_user

from exchange_rate_demo.forms import SignupForm


class DemoTestCase(TestCase):
    """ Demo tests
    """
    @classmethod
    def setUpTestData(cls):
        cls.data = {'username': 'user',
                    'email': 'user@user.com',
                    'password1': 'user',
                    'password2': 'user'}

    def setUp(self):
        self.client.post(reverse('signup'), self.data)
        self.client.get(reverse('logout'))

    def test_signup(self):
        """ Forms should work correctly
        """
        data = self.data
        data['username'] = 'user2'
        form = SignupForm(data=data)
        self.assertEqual(len(form.errors), 0)

        response = self.client.get(reverse('signup'))
        self.assertContains(response, "<h1>Signup</h1>", html=True)

        response = self.client.post(reverse('signup'), data)
        self.assertEqual(response.status_code, HTTPStatus.FOUND)
        self.assertEqual(response.url, '/')

        data['password2'] = 'different'
        response = self.client.post(reverse('signup'), data)
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertContains(response, "<h1>Signup</h1>", html=True)

    def test_home(self):
        """ Home view behaviour
        """
        data = self.data

        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, HTTPStatus.FOUND)
        self.assertEqual(response.url, '/login')

        self.client.post(reverse('login'), {'username': data['username'],
                                            'password': data['password1']})

        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertTrue('Welcome, user' in str(response.content))

        response = self.client.post(reverse('home'), {'username': data['username'],
                                                      'password': data['password1']})
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertTrue('<pre><code>{&#x27;token&#x27;:' in str(response.content))

        response = self.client.post(reverse('home'), {'username': data['username'],
                                                      'password': 'wrong_password'})
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertTrue('Get Token' in str(response.content))

    def test_logout(self):
        """ Logout should work
        """
        data = {'username': self.data['username'], 'password': self.data['password1']}
        self.client.post(reverse('login'), data)
        response = self.client.get(reverse('logout'))
        self.assertEqual(response.status_code, HTTPStatus.FOUND)
        self.assertEqual(response.url, '/')

    def test_login(self):
        """ Forms should work correctly
        """
        data = {'username': self.data['username'], 'password': self.data['password1']}

        response = self.client.get(reverse('login'))
        self.assertContains(response, "<h1>Login</h1>", html=True)

        response = self.client.post(reverse('login'), {'username': self.data['username'],
                                                       'password': 'wrong_password'})
        self.assertFalse(get_user(self.client).is_authenticated)
        self.assertContains(response, "<h1>Login</h1>", html=True)

        response = self.client.post(reverse('login'), data)
        self.assertEqual(response.status_code, HTTPStatus.FOUND)
        self.assertEqual(response.url, '/')

        data['password'] = 'wrong_password'
        response = self.client.post(reverse('login'), data)
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertContains(response, "<h1>Login</h1>", html=True)
