""" URLs module
"""
from django.urls import path
from django.urls.conf import include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static

from .views import signup_view, login_view, home_view, logout_request


urlpatterns = [
    path('admin/', admin.site.urls),
    path('signup', signup_view, name='signup'),
    path('login', login_view, name='login'),
    path('logout', logout_request, name='logout'),
    path('api/', include('api.urls')),
    path('', home_view, name='home')
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
