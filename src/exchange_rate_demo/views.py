""" Views module
"""
import json
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.forms import AuthenticationForm
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.renderers import JSONRenderer

from .forms import SignupForm


def signup_view(request):
    """ Signup view
    """
    form = SignupForm()
    if request.method == "POST":
        form = SignupForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect("home")

    return render(request=request, template_name="signup.html", context={"form": form})


def home_view(request):
    """ Home view
    """
    token = None

    if request.user.is_authenticated:
        if request.method == "POST":
            form = AuthenticationForm(request, data=request.POST)
            if form.is_valid():
                username = form.cleaned_data.get('username')
                password = form.cleaned_data.get('password')
                user = authenticate(username=username, password=password)
                if user is not None:
                    response = obtain_auth_token(request=request)
                    response.accepted_renderer = JSONRenderer()
                    response.accepted_media_type = "application/json"
                    response.render()
                    token = json.loads(response.content)

        if request.method == "GET":
            form = AuthenticationForm(initial={'username': request.user.username})

        field = form.fields['username']
        field.widget = field.hidden_widget()

        return render(request=request,
                      template_name="home.html",
                      context={"form": form, "token": token})

    return redirect("login")


def logout_request(request):
    """ Logout request view
    """
    logout(request)
    return redirect("home")


def login_view(request):
    """ Login view
    """
    if request.method == "POST":
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')

            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("home")

    form = AuthenticationForm()
    return render(request=request, template_name="login.html", context={"form": form})
