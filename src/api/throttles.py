""" API throttle module
"""
from rest_framework.throttling import UserRateThrottle


class BurstRateThrottle(UserRateThrottle):
    """ Burst Throtttle scope class
    """
    scope = 'burst'


class SustainedRateThrottle(UserRateThrottle):
    """ Sustained Throtttle scope class
    """
    scope = 'sustained'
