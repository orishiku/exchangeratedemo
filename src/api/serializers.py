""" Serializers module
"""
from rest_framework import serializers
from django.contrib.auth import get_user_model
from sources.models import Rate


class UserSerializers(serializers.ModelSerializer):
    """ User serializer
    """
    class Meta:
        model = get_user_model()
        fields = '__all__'


class RateSerializer(serializers.ModelSerializer):
    """ Rate serializer
    """
    class Meta:
        model = Rate
        fields = ['source', 'last_update', 'value']
