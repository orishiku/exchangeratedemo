""" Tests module
"""

from http import HTTPStatus
from django.test import TestCase
from django.urls.base import reverse
from django.contrib.auth import get_user_model
from rest_framework.test import APIRequestFactory, force_authenticate

from api.viewsets import RateViewSet


class APITestCase(TestCase):
    """ API tests class
    """
    @classmethod
    def setUpTestData(cls):
        cls.data = {'username': 'user',
                    'email': 'user@user.com',
                    'password1': 'user',
                    'password2': 'user'}

    def setUp(self):
        self.client.post(reverse('signup'), self.data)
        self.client.post(reverse('home'), {'username': self.data['username'],
                                           'password': self.data['password1']})

        user = get_user_model()
        self.factory = APIRequestFactory()
        self.user = user.objects.get(username=self.data['username'])
        self.view = RateViewSet.as_view({'get': 'list'})

    def test_rates_endpoint(self):
        """ rates endpoint should return current rates from 3 sources
        """
        request = self.factory.get('/api/rates/')
        force_authenticate(request, user=self.user, token=self.user.auth_token)

        response = self.view(request)
        self.assertEqual(response.status_code, HTTPStatus.OK)
        self.assertNumQueries(3)
        sources = []
        for source in response.data:
            self.assertTrue('source' in source and 'last_update' in source and 'value' in source,
                            'source data is incomplete')
            if source['source'] not in sources:
                sources.append(source['source'])

        self.assertEqual(len(sources), 3, 'Not all sources has been retrived')
