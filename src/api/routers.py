""" Routers module
"""
from rest_framework import routers

from api.viewsets import UserViewset, RateViewSet


router = routers.DefaultRouter()
router.register('users', UserViewset, basename='user_api')
router.register('rates', RateViewSet, basename='rate_api')
