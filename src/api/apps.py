""" API apps module
"""
from django.apps import AppConfig


class ApiConfig(AppConfig):
    """ api configuration
    """
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'api'
