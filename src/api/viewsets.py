""" Viewsets module
"""
from django.utils import timezone
from django.contrib.auth import get_user_model
from rest_framework import viewsets

from api.serializers import UserSerializers, RateSerializer
from sources.models import Rate, RateSources
from sources.core.dof import SourceDOF
from sources.core.fixer import SourceFixer
from sources.core.banxico import SourceBanxico


class UserViewset(viewsets.ModelViewSet):
    """ User viewset
    """
    User = get_user_model()
    queryset = User.objects.all()
    serializer_class = UserSerializers


class RateViewSet(viewsets.ModelViewSet):
    """ Rates viewset
    """
    serializer_class = RateSerializer

    def get_queryset(self):
        datetime = timezone.now()
        datetime = datetime.strftime("%Y-%m-%d")
        rate_query = Rate.objects.filter(source_date=datetime)

        if len(rate_query.filter(source=RateSources.DOF)) == 0:
            source = SourceDOF()
            rate = source.current_rate()

            Rate.objects.create(source=RateSources.DOF,
                                source_date=rate['date'],
                                value=rate['value'])

        if len(rate_query.filter(source=RateSources.FIXER)) == 0:
            source = SourceFixer()
            rate = source.current_rate()

            Rate.objects.create(source=RateSources.FIXER,
                                source_date=rate['date'],
                                value=rate['value'])

        if len(rate_query.filter(source=RateSources.BANXICO)) == 0:
            source = SourceBanxico()
            rate = source.current_rate()

            Rate.objects.create(source=RateSources.BANXICO,
                                source_date=rate['date'],
                                value=rate['value'])

            rate_query = Rate.objects.filter(source_date=datetime)

        return Rate.objects.all().order_by('-last_update')[:3]
