""" urls module
"""
from django.urls.conf import path, include
from rest_framework.authtoken import views

from api.routers import router


app_name = 'api'
urlpatterns = [
    path('', include(router.urls)),
    path('token-auth/', views.obtain_auth_token, name='token-auth')
]
