FROM python:3.9-buster

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

ENV APP_PATH /app
ENV VIRTUAL_ENV /env
ENV PATH /env/bin:$PATH

RUN mkdir -p $APP_PATH/static
ADD requirements_build.txt $APP_PATH/requirements_build.txt
ADD requirements.txt $APP_PATH/requirements.txt
ADD src $APP_PATH

RUN set -ex \
    && python -m venv /env \
    && /env/bin/pip install --upgrade pip \
    && /env/bin/pip install --no-cache-dir gunicorn \
    && /env/bin/pip install --no-cache-dir -r /app/requirements_build.txt \
    && /env/bin/pip install --no-cache-dir -r /app/requirements.txt

WORKDIR $APP_PATH

EXPOSE 8000

ENTRYPOINT ["sh", "start.sh"]